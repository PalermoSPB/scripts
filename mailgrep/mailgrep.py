#!/usr/bin/env python3
import argparse
import re
import socket # for gethostname

def domain_to_file(args):

    if args.verbose:
      print("reading %s..." % args.infile)

    if args.infile and (args.deferred or args.bounced):
      status_mail = ("status=" + (args.deferred or args.bounced),)
    else:
      status_mail = ("status=deferred", "status=bounced",)
    unique = {}
    with open(args.infile, 'r') as opened_infile:
      for line in opened_infile:
        if any ([s for s in status_mail if s in line and args.exclude not in line]):
          if args.domain == "domain":
            deferred = line.split('to=<')[1]
            deferred = deferred.split('>')[0]
            deferred = deferred.split('@')[1]
          else:
            deferred = line.split('<')[1]
            deferred = deferred.split('>')[0]
          if deferred not in unique.keys():
            unique[deferred] = 1
          else:
            unique[deferred] += 1

      if args.numeric == "numeric":
        with open(args.outfile, 'w') as opened_file:
          for mail, value in sorted(unique.items(), key=lambda e: e[1], reverse=True):
            opened_file.write("{0}  {1}\n".format(mail, value))
          num = str(sum(1 for l in unique))
          print("email: " + num)
      else:
        with open(args.outfile, 'w') as opened_file:
          for mail in sorted(unique, key=lambda e: e[0] ):
            opened_file.write("{0}\n".format(mail))
          num = str(sum(1 for l in unique))
          print("unique email: " + num)

def createParser():
    hostname = socket.gethostname() # identify hostname
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--infile", action="store", type=str, dest="infile", help="Input FILE", metavar="FILE") # Input file for parser 
    parser.add_argument("-o", "--outfile", action="store", default="outfile.txt", dest="outfile", help="write report to FILE", metavar="FILE") # Output file
    parser.add_argument("-q", "--quiet", action="store_false", dest="verbose", default=True, help="don't print status messages to stdout")
    parser.add_argument("-D","--domain", action="store_const", const="domain", help="Output into file domain of e-mail") # Only domain output 
    parser.add_argument("-b", "--bounced", action="store_const", const="bounced", help="Only bounced e-mail")
    parser.add_argument("-d", "--deferred", action="store_const", const="deferred", help="Only deferred e-mail")
    parser.add_argument("-n", "--numeric", action="store_const", const="numeric", help="Numeric of e-mail or domain into file")
    parser.add_argument("-x", "--exclude", action="store", type=str, default=hostname, help="exclude domain into file")
    return  parser.parse_args()

def main():
    args = createParser()

    if args.infile == None:
      parser = argparse.ArgumentParser()
      parser.error("Select Input File")
    else:
      domain_to_file(args)

if __name__ == "__main__":
    main()

usage: mailgrep.py [-h] [-i FILE] [-o FILE] [-q] [-D] [-b] [-d] [-n]
                   [-x EXCLUDE]
                   
optional arguments:

  -h, --help            show this help message and exit

  -i FILE, --infile FILE Input FILE

  -o FILE, --outfile FILE write report to FILE
  
  -q, --quiet           don't print status messages to stdout
  
  -D, --domain          Output into file domain of e-mail
  
  -b, --bounced         Only bounced e-mail
  
  -d, --deferred        Only deferred e-mail
  
  -n, --numeric         Numeric of e-mail or domain into file
  
  -x EXCLUDE, --exclude EXCLUDE exclude domain into file
